def readFile(i):
    gradesFile.seek(0)
    line = gradesFile.readlines()[i]
    line = line.rstrip("\n")
    line = line.split(",")
    foreName = line.pop(0)
    surName = line.pop(0)
    passGrade = line.count("P")
    meritGrade = line.count("M")
    distictionGrade = line.count("D")
    points = int(passGrade)*70+int(meritGrade)*80+int(distictionGrade)*90
    return [foreName,surName,points]

def gradeCalculation(points):
    g=0
    u=0
    gradeDictionary = {range(1,1300):"PPP",range(1300,1340):"MPP",range(1340,1380):"MMP",range(1380,1420):"MMM",range(1420,1460):"DMM",range(1460,1500):"DDM",range(1500,1530):"DDD",range(1530,1560):"D*DD",range(1560,1690):"D*D*D",range(1590,1621):"D*D*D*"}
    ucasDictionary = {range(1,1300):"48",range(1300,1340):"64",range(1340,1380):"80",range(1380,1420):"96",range(1420,1460):"112",range(1460,1500):"128",range(1500,1530):"144",range(1530,1560):"152",range(1560,1690):"160",range(1590,1621):"168"}
    for key in gradeDictionary:
        if points in key:
            g = gradeDictionary[key]
    for key in ucasDictionary:
        if points in key:
            u = ucasDictionary[key]
    return [g, u]

def writeFile(foreName, surName, points, grade, ucas):
    if points == 0: 
        return
    outputGrades.write(foreName+", "+surName+", "+str(points)+", "+grade+", "+ucas+"\n") 
    
outputPath = "GradesOutput.csv"
inputPath = "Grades.csv"
gradesFile = open(inputPath, 'r')
outputGrades = open(outputPath, 'w')
rows = sum(1 for row in gradesFile)
outputGrades.write("Forename, Surname, Credits, Grade, UCAS\n")

for i in range (0, rows):
    pointsNames = readFile(i)
    grade = gradeCalculation(pointsNames[2]) 
    writeFile(pointsNames[0],pointsNames[1],pointsNames[2],grade[0],grade[1])

gradesFile.close() 
outputGrades.close()

