# Grades Calculator

This program was written as part of my first programming assignment during my Access to Computing in HE course.

It takes the source file "Grades.csv", a file containing the module results of several students, and calculates the
students forename, surname, total credits, overall grade and the UCAS points they achieved on their course.

For the program to run successfully, the "Grades.csv" file and the program file must be in the same directory.

I achieved a Distinction grade overall for this module.